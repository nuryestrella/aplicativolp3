 $( ".modal" ).each(function(index) {
                 $(this).on('show.bs.modal', function (e) {
                     var open = $(this).attr('data-easein');
                         if(open == 'shake') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                } else if(open == 'pulse') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                } else if(open == 'tada') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                } else if(open == 'flash') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                }  else if(open == 'bounce') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                } else if(open == 'swing') {
                                     $('.modal-dialog').velocity('callout.' + open);
                                }else {
                                  $('.modal-dialog').velocity('transition.' + open);
                                }
                 });
            });
			$(document).ready(function() {
				var table = $('#table-example').dataTable({
                    "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }

                    }
                }

                );
			});