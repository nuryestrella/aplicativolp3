  var animating = false,
      submitPhase1 = 1100,
      submitPhase2 = 400,
      logoutPhase1 = 800,
      $login = $(".login"),
      $app = $(".app");

  function ripple(elem, e) {
    $(".ripple").remove();
    var elTop = elem.offset().top,
        elLeft = elem.offset().left,
        x = e.pageX - elLeft,
        y = e.pageY - elTop;
    var $ripple = $("<div class='ripple'></div>");
    $ripple.css({top: y, left: x});
    elem.append($ripple);
  };

  $(document).on("click", ".app__logout", function(e) {
    if (animating) return;
    $(".ripple").remove();
    animating = true;
    var that = this;
    $(that).addClass("clicked");
    setTimeout(function() {
      $app.removeClass("active");
      $login.show();
      $login.css("top");
      $login.removeClass("inactive");
    }, logoutPhase1 - 120);
    setTimeout(function() {
      $app.hide();
      animating = false;
      $(that).removeClass("clicked");
    }, logoutPhase1);
  });


$("form").submit(function () {

    w=$(this).serialize();
    $.post("",w,function(data){
        unblock_form();
        location.reload();
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            if (e == '__all__'){
                $('#msm').html(errors[e]);
            }
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
    return false; 
});

function unblock_form() {
        $('.errorlist').remove();
    }
