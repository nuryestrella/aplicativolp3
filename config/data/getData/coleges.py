import json
from django.db import transaction, IntegrityError
from django.db.models import Count

from apps.admacademico.models import coleges


#aa = colegios.objects.all().order_by('dre').values('dre').annotate(dres=Count('dre'))
#print(aa.query)

def colege():
    try:
        with transaction.atomic():
            if len(coleges.objects.all()) == 0:
                with open('json/coleges.json', encoding="UTF-8") as f:
                    col = json.load(f)

                for i in col:
                    c = coleges(**i)
                    c.save()
                    print("se agrego el colegio " + c.name)

            else:
                raise ValueError("Hay colegios ingresados, limpia la base de datos")

    except IntegrityError as e:
        error = str(e)
        print(error)
