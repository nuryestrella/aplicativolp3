from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.security.urls')),
    url(r'^', include('apps.Sistema_Aplicacion.urls')),
    url(r'^', include('apps.appmatricula.urls')),
    url(r'^', include('apps.appAsignaturaDocente.urls')),
    url(r'^', include('apps.appReporte.urls')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
