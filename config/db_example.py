DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'testpsicologico',
       'USER'    : 'testpsicologico',
       'PASSWORD': 'testpsicologico',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}