## software
software Es una plataforma web en la nube, que automatiza los procesos de comercialización de bienes/servicios de cualquier rubro.

### Version
V1

### Tegnologías

* [Django1.9] - Django hace que sea más fácil de construir mejores aplicaciones web más rápido y con menos código.

### Installation

$ git clone
$ pip install -r requirements.txt


### Configurar Base de Datos `software/config/db_example.py`

copiar db_example.py a local.py
```sh
$  cp software/config/db_example.py software/config/db.py
```
<br>
DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'software',
       'USER'    : 'software',
       'PASSWORD': 'software',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}
```

### Migraciones [warning]

```sh
$  python manage.py makemigrations
$  python manage.py migrate
```

### Crear Super Usuario
```sh
$  python manage.py createsuperuser
```

### agregar data inicial
```sh
$  python start.py
```

### Iniciar
```sh
$ python manage.py runserver www.software.edu.pe
```

### Desarrollo

¿Quieres contribuir? ¡Excelente!

Abra su terminal favorito y ejecutar estos comandos de la instalación.

### Todos

- Pruebas de escritura
- Repensar Github Guardar
- Añadir Código Comentarios
- Añadir Modo nocturno

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [@thomasfuchs]: <http://twitter.com/thomasfuchs>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [marked]: <https://github.com/chjj/marked>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [keymaster.js]: <https://github.com/madrobby/keymaster>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [Django1.9]: <https://www.djangoproject.com>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]:  <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>


