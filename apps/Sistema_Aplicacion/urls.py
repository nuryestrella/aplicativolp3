from django.conf.urls import url
#from .views import
from .views import (EliminarSemestre,EliminarAsignatura,EliminarAula,litarDocente,litarSemestre,litarAsignatura,litarAula,
                    addDocente,FormSemestre,FormAsignatura,FormAula,
                    UpdateSemestre,UpdateAsignatura,UpdateAula,litarAlumno, addAlumno, UpdateAlumno, EliminarAlumno)

urlpatterns = [
     url(r'^registro/RegistroAlumnos$', litarAlumno),
     url(r'^registro/update/(?P<pk>[0-9]+)/', UpdateAlumno.as_view()),
     url(r'^registro/FormularioAlumno/$', addAlumno.as_view()),
     url(r'^registro/Alumno/eliminar', EliminarAlumno),

     url(r'^registro/RegistroDocente$', litarDocente),
     url(r'^registro/FormularioDocente/$', addDocente.as_view()),


     url(r'^registro/RegistroSemestre$', litarSemestre),
     url(r'^registro/FormularioSemestre$', FormSemestre),
     url(r'^registro/Semestre/update', UpdateSemestre),
     url(r'^registro/Semestre/eliminar', EliminarSemestre),


     url(r'^registro/RegistroAsignatura$',litarAsignatura),
     url(r'^registro/FormularioAsignatura$', FormAsignatura),
     url(r'^registro/Asignatura/update', UpdateAsignatura),
     url(r'^registro/Asignatura/eliminar', EliminarAsignatura),

     url(r'^registro/RegistroAula$',litarAula),
     url(r'^registro/FormularioAula$',FormAula),
     url(r'^registro/Aula/update', UpdateAula),
     url(r'^registro/Aula/eliminar', EliminarAula),
]