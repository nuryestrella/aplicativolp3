from django.db import models
from apps.security.models import Users

# Create your models here.

class Asignatura (models.Model):
    Nombre          =models.CharField(max_length=50)
    Creditos        =models.IntegerField(default=0)
    Estado          =models.BooleanField(default=True)

    def __str__(self):
        return self.Nombre
    class Meta:
        db_table ="Asignatura"

class Semestre (models.Model):
    Anio_semestre               =models.CharField(max_length=50)
    Descripcion                 =models.CharField(max_length=60)
    Estado                      =models.BooleanField(default=True)
    #asignatura                  =models.ManyToManyField(Asignatura, through='AsignaturaxDocente')
    docente                     =models.ManyToManyField(Users, through='SemestrexDocente')
    def __str__(self):
        return self.Anio_semestre
    class Meta:
        db_table ="Semestre"


class SemestrexDocente(models.Model):
    idAsignatura          = models.ManyToManyField(Asignatura, through='DocentexAsignatura')
    idDocente              = models.ForeignKey(Users,verbose_name='Docente', null=True)
    idSemestre             = models.ForeignKey(Semestre,verbose_name='Semestre')
    Estado                 = models.BooleanField(default=True)
    class Meta:
        db_table ="SemestrexDocente"

class DocentexAsignatura(models.Model):
    TipoEnsenianza         = models.CharField(max_length=30, null=True)
    idSemestrexDocente   = models.ForeignKey(SemestrexDocente)
    idAsignatura           = models.ForeignKey(Asignatura)
    Estado                 = models.BooleanField(default=True)
    class Meta:
        db_table ="DocentexAsignatura"

class Aula (models.Model):
    Descripcion              =models.CharField(max_length=30,verbose_name='Aula')
    asignaturadocente        =models.ManyToManyField(DocentexAsignatura, through="Clases")
    Estado                   =models.BooleanField(default=True)
    def __str__(self):
        return str(self.Descripcion)
    class Meta:
        db_table ="Aula"

class Clases(models.Model):
    Tema                                =models.CharField(max_length=50)
    Descripcion                         =models.CharField(max_length=100)
    fecha                               =models.DateField()
    Estado                              =models.BooleanField(default=True)
    idDocentexAsignatura                =models.ForeignKey(DocentexAsignatura)
    idAula                              =models.ForeignKey(Aula)
    def __str__(self):
        return self.Tema
    class Meta:
        db_table ="Clases"


class Matriculados (models.Model):
    codMatricula        =models.CharField(max_length=8, null=True, blank=True)
    idAlumno            =models.ForeignKey(Users,verbose_name='Alumno', null=True)
    idDocentexAsignatura   =models.ForeignKey(DocentexAsignatura)
    Estado              =models.BooleanField(default=True)
    clase               =models.ManyToManyField(Clases,through='Asistencia',related_name="fk_Asistencia")
    #evaluacion          =models.ManyToManyField(Clases,through='Evaluacion',related_name="fk_Evaluacion")
    def __str__(self):
        return self.codMatricula
    class Meta:
        db_table ="Matriculados"

class Asistencia (models.Model):
    idmatriculados = models.ForeignKey(Matriculados)
    idclases       = models.ForeignKey(Clases)
    fecha          = models.DateTimeField(auto_now_add=True)
    Estado              =models.BooleanField(default=True)
    class Meta:
        db_table ="Asistencia"


class Evaluacion (models.Model):
    primerapractica  =models.IntegerField(default=0)
    segundapractica  =models.IntegerField(default=0)
    primerparcial    =models.IntegerField(default=0)
    segundoparcial   =models.IntegerField(default=0)
    trabajos         =models.IntegerField(default=0)
    idmatriculados   = models.ForeignKey(Matriculados)

    class Meta:
        db_table ="Evaluacion"
