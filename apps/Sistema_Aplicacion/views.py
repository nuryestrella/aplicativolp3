from django.shortcuts import render, HttpResponse, get_object_or_404
from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from registration.backends.hmac.views import RegistrationView
from django.views.generic import UpdateView
from django.http import HttpResponseBadRequest
from django.core.mail import send_mail

from .models import Asignatura,Semestre,DocentexAsignatura,Aula,Evaluacion,Clases,Asistencia,Matriculados
from apps.security.models import Users
from apps.core.error import error
from .forms import formAlumno,formSemestre,formAsignatura,formAula
import json, string, random
# Create your views here.

#jalar de la base de datos las tablas
'''
    clas=Clases.objects.filter(Estado=True)
    matr=Matriculados.objects.filter(Estado=True)
    eval=Evaluacion.objects.filter(Estado=True)
    asigxdoce=DocentexAsignatura.objects.all()'''


def litarAlumno(r):
     alum= Users.objects.filter(status=True, profiles=10)
     return render(r,'listaalumnos.html',{'alumno':alum})

#contraseña aleatoria
def id_generator(size=8, chars=string.ascii_uppercase + string.digits + string.punctuation ):
    return ''.join(random.choice(chars) for _ in range(size))


class addAlumno(LoginRequiredMixin, RegistrationView):
    login_url= reverse_lazy('error404')
    form_class  = formAlumno
    template_name = "formularioAlumno.html"

    def get_context_data(self, **kwargs):
        context = super(addAlumno, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        return context

    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):
        p = id_generator()
        new_user                   = form.save(commit=False)
        new_user.is_active         = True
        new_user.username          = form.cleaned_data["dni"]
        new_user.email             = form.cleaned_data["email"]
        new_user.password_default  = p
        new_user.set_password(p)
        new_user.save()
        new_user.profiles.add(10)
        send_mail(
            'SISALUMNO le da la bienvenida!',
            'su usuario es: .'+str(form.cleaned_data["dni"])+' y su contraseña es: '+str(p),
            'sisalumno@gmail.com',
            [form.cleaned_data["email"]],
            fail_silently=False,
        )
        return new_user

class UpdateAlumno(LoginRequiredMixin, UpdateView):
    login_url= reverse_lazy('error404')
    template_name = "formularioAlumno.html"
    model = Users
    form_class  = formAlumno
    exclude = ('password1','password1')
    success_url = reverse_lazy('user-completado')

    def get_context_data(self, **kwargs):
        context = super(UpdateAlumno, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        return context

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

def EliminarAlumno(em):
    idd = int(em.GET.get('iddd'))
    e = Users.objects.get(pk = idd)
    e.status = False
    e.save()
    return HttpResponse("eliminado correctamente")


####################################################################################################

def litarDocente(r):
     doce= Users.objects.filter(status=True, profiles=11).order_by("-id")
     return render(r,'listadocente.html',{'docente':doce})

class addDocente(LoginRequiredMixin, RegistrationView):
    login_url= reverse_lazy('error404')
    form_class  = formAlumno
    template_name = "formularioDocente.html"

    def get_context_data(self, **kwargs):
        context = super(addDocente, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        return context

    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):
        p = id_generator()
        new_user                   = form.save(commit=False)
        new_user.is_active         = True
        new_user.username          = form.cleaned_data["dni"]
        new_user.email             = form.cleaned_data["email"]
        new_user.password_default  = p
        new_user.set_password(p)
        new_user.save()
        new_user.profiles.add(11)
        send_mail(
            'SISALUMNO le da la bienvenida!',
            'su usuario es: .'+str(form.cleaned_data["dni"])+' y su contraseña es: '+str(p),
            'sisalumno@gmail.com',
            [form.cleaned_data["email"]],
            fail_silently=False,
        )
        return new_user

################################################################################################################
def litarSemestre(r):
     seme=Semestre.objects.filter(Estado=True)
     return render(r,'listaSemestre.html',{'Semestre':seme})

def FormSemestre(r):
    if r.method =='GET':
            f = formSemestre()
            t = "REGISTRAR"
    else:
        f =formSemestre(r.POST)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))
    return render(r,"formularioSemestre.html",{'tipo':t,'formulario':f})

def UpdateSemestre(r):
    if r.method =='GET':
            idd = int(r.GET.get('idd'))
            a = get_object_or_404(Semestre,pk=idd)
            t = "ACTUALIZAR"
            f = formSemestre(instance=a)
            return render(r,"formularioSemestre.html",{'tipo':t,'formulario':f,'idd':idd})
    else:
        idd = int(r.POST.get('idd'))
        a = get_object_or_404(Semestre,pk=idd)
        f =formSemestre(r.POST, instance=a)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))

def EliminarSemestre(em):
    idd = int(em.GET.get('iddd'))
    e = Semestre.objects.get(pk = idd)
    e.Estado = False
    e.save()
    return HttpResponse("eliminar correctamente")

####################################################################################################
def litarAsignatura(r):
     asig = Asignatura.objects.filter(Estado=True)
     return render(r,'listaAsignatura.html',{'asignatura':asig})

def FormAsignatura(r):
    if r.method =='GET':
            f = formAsignatura()
            t = "REGISTRAR"
    else:
        f =formAsignatura(r.POST)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))
    return render(r,"formularioAsignatura.html",{'tipo':t,'formulario':f})
def UpdateAsignatura(r):
    if r.method =='GET':
            idd = int(r.GET.get('idd'))
            a = get_object_or_404(Asignatura,pk=idd)
            t = "ACTUALIZAR"
            f = formAsignatura(instance=a)
            return render(r,"formularioAsignatura.html",{'tipo':t,'formulario':f,'idd':idd})
    else:
        idd = int(r.POST.get('idd'))
        a = get_object_or_404(Asignatura,pk=idd)
        f =formAsignatura(r.POST, instance=a)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))

def EliminarAsignatura(em):
    idd = int(em.GET.get('iddd'))
    e = Asignatura.objects.get(pk = idd)
    e.Estado = False
    e.save()
    return HttpResponse("eliminar correctamente")

##########################################################################################################################
def litarAula(r):
     aula=Aula.objects.filter(Estado=True)
     return render(r,'listarAula.html',{'aula':aula})

def FormAula(r):
    if r.method =='GET':
            f = formAula()
            t = "REGISTRAR"
    else:
        f =formAula(r.POST)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))
    return render(r,"formularioAula.html",{'tipo':t,'formulario':f})

def UpdateAula(r):
    if r.method =='GET':
            idd = int(r.GET.get('idd'))
            a = get_object_or_404(Aula,pk=idd)
            t = "ACTUALIZAR"
            f = formAula(instance=a)
            return render(r,"formularioAula.html",{'tipo':t,'formulario':f,'idd':idd})
    else:
        idd = int(r.POST.get('idd'))
        a = get_object_or_404(Aula,pk=idd)
        f =formAula(r.POST, instance=a)
        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))


def EliminarAula(em):
    idd = int(em.GET.get('iddd'))
    e = Aula.objects.get(pk = idd)
    e.Estado = False
    e.save()
    return HttpResponse("eliminado correctamente")


###############################REPORTESSS####################################################################
#def NotaxAsis (r):

   # return render(r,'NotaxAsistencia.html',{})



























