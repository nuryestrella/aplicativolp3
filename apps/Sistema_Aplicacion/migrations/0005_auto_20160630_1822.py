# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-06-30 23:22
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Sistema_Aplicacion', '0004_remove_matriculados_idalumno'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Alumnos',
        ),
        migrations.AddField(
            model_name='matriculados',
            name='idAlumno',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Alumno'),
        ),
    ]
