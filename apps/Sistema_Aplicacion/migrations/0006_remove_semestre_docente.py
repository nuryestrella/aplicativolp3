# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-07-02 16:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Sistema_Aplicacion', '0005_auto_20160630_1822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='semestre',
            name='docente',
        ),
    ]
