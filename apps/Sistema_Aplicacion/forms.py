from django import forms
from .models import Semestre,Asignatura,Aula, Users

S = (
    ('m', 'Masculino'),
    ('f', 'Femenino'),
)
def listF(a, b):
    d = {'class': 'form-control input-sm'}
    d[a] = b
    return d

class formAlumno(forms.ModelForm):
    password1 = forms.CharField(label=("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Password confirmation"), widget=forms.PasswordInput,  help_text=("Enter the same password as before, for verification."))

    email = forms.EmailField(
        help_text=('Email'),
        required=False,
        widget= forms.EmailInput(attrs={'class': 'form-control input-sm','placeholder':'ingresar dirección de correo Electrónico'}),
    )

    sex = forms.ChoiceField(
        choices = S,
        label="Sexo",
        widget=forms.Select(attrs=listF('placeholder','')))

    #email = forms.CharField(widget=forms.EmailInput(),required=False)
    password1 = forms.CharField(widget=forms.HiddenInput(),required=False)
    password2 = forms.CharField(widget=forms.HiddenInput(),required=False)
    username = forms.CharField(widget=forms.HiddenInput(),required=False)

    class Meta:
        model = Users
        fields  = ('names','first_name','last_name','username','dni','sex','email')
        #exclude = ('password1','password1','email',)

        widgets = {

            'names'     : forms.TextInput(attrs=listF('placeholder','ingrese nombres completos')),
            'first_name': forms.TextInput(attrs=listF('placeholder','ingrese apellido paterno')),
            'last_name' : forms.TextInput(attrs=listF('placeholder','ingrese apellido materno')),
            'dni'       : forms.TextInput(attrs=listF('placeholder','ingrese DNI')),
        }


    def __init__(self, *args, **kwargs):
        super(formAlumno, self).__init__(*args, **kwargs)
        self.fields.pop('username')
        #self.fields.pop('email')
        self.fields.pop('password1')
        self.fields.pop('password2')

class formSemestre(forms.ModelForm):
    class Meta():
        model = Semestre
        filter = '__all__'
        exclude = ('Estado','asignatura','docente')
        widgets = {
            'Anio_semestre' : forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese año del semestre'}),
            'Descripcion' : forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese su Descripcion'}),
        }

class formAsignatura(forms.ModelForm):
    class Meta():
        model   = Asignatura
        filter  ='__all__'
        exclude = ('Estado',)
        widgets = {
            'Nombre' : forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese su nombre'}),
            'Creditos': forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese el Credito del Curso'}),

        }

class formAula(forms.ModelForm):
    class Meta():
        model   = Aula
        filter  ='__all__'
        exclude = ('Estado','asignaturadocente')
        widgets = {
            'Descripcion' : forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese Aula'}),
        }