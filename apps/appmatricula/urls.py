from django.conf.urls import url
from .views import (registromatricula,verclase,vernotasalum)

urlpatterns = [
     url(r'^gestionmatricula/matricula$', registromatricula),
     url(r'^gestionmatricula/verclase', verclase),
     url(r'^gestionmatricula/marcarasistencia', verclase),
     url(r'^gestionmatricula/vernotasalum', vernotasalum),

]