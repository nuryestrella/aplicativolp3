from django.shortcuts import render, HttpResponse, get_object_or_404
from django.http import HttpResponseBadRequest
from apps.Sistema_Aplicacion.models import Asistencia,DocentexAsignatura,Semestre, Matriculados, Clases, Evaluacion
import json
# Create your views here.

def registromatricula(r):

     if r.method == 'GET':
         idu = int(r.user.id)
         m = Matriculados.objects.filter(idAlumno_id=idu, Estado=True)[:1]
         ids = Semestre.objects.filter(Estado=True).order_by('-id').values('id')[0]
         if m:
             d= Matriculados.objects.filter(idAlumno_id=idu,Estado=True,
                                            idDocentexAsignatura__Estado=True,
                                            idDocentexAsignatura__idSemestrexDocente__Estado=True,
                                            idDocentexAsignatura__idSemestrexDocente__idDocente__status=True,
                                            idDocentexAsignatura__idAsignatura__Estado=True,
                                            idDocentexAsignatura__idSemestrexDocente__idSemestre_id=ids['id']).values(
                 'idDocentexAsignatura__idAsignatura__Nombre',
                 'idDocentexAsignatura__idAsignatura__Creditos',
                 'idDocentexAsignatura__idAsignatura_id',
                 'idDocentexAsignatura_id',
                 'idDocentexAsignatura__idSemestrexDocente__idDocente__first_name',
                 'idDocentexAsignatura__idSemestrexDocente__idDocente__last_name',
                 'idDocentexAsignatura__idSemestrexDocente__idDocente__names'
             )
             return render(r,"matriculado.html",{'cursos':d})

         else:
             d = DocentexAsignatura.objects.filter(Estado=True,idSemestrexDocente__Estado=True,
                                                   idSemestrexDocente__idDocente__status=True,
                                                   idAsignatura__Estado=True,
                                                   idSemestrexDocente__idSemestre_id=ids['id']).values(
                 'idAsignatura__Nombre','idAsignatura__Creditos','idAsignatura_id',
                 'idSemestrexDocente__idDocente__first_name',
                 'idSemestrexDocente__idDocente__last_name',
                 'idSemestrexDocente__idDocente__names',
                 'id',
             )
             return render(r,"matricula.html",{'matricula':d})

     else:

         for i in r.POST.getlist('idcurso[]'):
             m = Matriculados()
             m.idDocentexAsignatura_id = int(i)
             m.idAlumno_id = int(r.user.id)
             m.save()

         return HttpResponse("ok")

from datetime import datetime
def verclase(r):
    if r.method =="POST":
        idm = Matriculados.objects.filter(Estado=True, idAlumno_id=int(r.user.id)).values('id').order_by('-id')[0]
        s = Asistencia.objects.filter(idclases_id=int(r.POST.get('id')),idmatriculados_id=idm['id'], Estado=True)

        if s:
            msm ="ya se encuentra guardado"
            return HttpResponseBadRequest(msm)
        else:
            Asistencia(idclases_id=int(r.POST.get('id')),idmatriculados_id=idm['id']).save()
            msm ="guardado correctamente"
            return HttpResponse(msm)

    else:
        d = Clases.objects.filter(Estado=True, idDocentexAsignatura_id=int(r.GET.get('id'))).values(
                'Tema','id','Descripcion',
                'fecha', 'idAula__Descripcion'
                 )
        a = Asistencia.objects.filter(Estado=True)
        idm = Matriculados.objects.filter(Estado=True, idAlumno_id=int(r.user.id)).values('id').order_by('-id')[0]

        return render(r,"verclase.html",{'clases':d,'asistencia':a,'ida':r.GET.get('id'),'idm':idm['id'],'datet':datetime.now().strftime("%d-%m-%Y")})


def vernotasalum(r):
    idm = Matriculados.objects.filter(Estado=True,idDocentexAsignatura_id=int(r.GET.get("idd")), idAlumno_id=int(r.user.id)).values('id').order_by('-id')[0]
    evv = Evaluacion.objects.filter(idmatriculados__id=int(idm['id']))
    print(idm)
    eva = []
    for j in evv:
        eva.append({
                    'id': j.id,
                    'primerapractica'      : j.primerapractica,
                    'segundapractica'      : j.segundapractica,
                    'primerparcial'      : j.primerparcial ,
                    'segundoparcial'      : j.segundoparcial,
                    'trabajos'      : j.trabajos,
                    'notafinal'      : float(j.primerapractica *  0.15 + j.segundapractica *  0.15 + j.primerparcial *  0.25 + j.segundoparcial *  0.25 + j.trabajos *  0.20),
                })
    print(eva)
    return render(r,"listanota.html",{'eva':eva})

