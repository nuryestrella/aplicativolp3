from django.shortcuts import render
from apps.Sistema_Aplicacion.models import DocentexAsignatura, Semestre, Matriculados, SemestrexDocente


# Create your views here.

def listadoc(r):
    template = "listadoc.html"
    ids = Semestre.objects.filter(Estado=True).order_by('-id').values('id')[0]
    d = DocentexAsignatura.objects.filter(Estado=True,idSemestrexDocente__Estado=True,
                                                   idSemestrexDocente__idDocente__status=True,
                                                   idAsignatura__Estado=True,
                                                   idSemestrexDocente__idSemestre_id=ids['id']).values(
                 'idAsignatura__Nombre','idAsignatura__Creditos','idAsignatura_id',
                 'idSemestrexDocente__idDocente__first_name',
                 'idSemestrexDocente__idDocente__last_name',
                 'idSemestrexDocente__idDocente__names',
                 'id',
             )
    m = Matriculados.objects.filter(Estado=True).values(
            'idAlumno__names','id','idAlumno__first_name',
        'idAlumno__last_name', 'idAlumno__dni','idDocentexAsignatura_id'
        )

    return render(r, template, {'doc':d,'mat':m})

def listaasignatura(r):
    template = "listaasig.html"
    ids = Semestre.objects.filter(Estado=True).order_by('-id').values('id')[0]
    d = SemestrexDocente.objects.filter(Estado=True, idSemestre_id=ids['id']).values(
                 'idDocente__first_name',
                 'idDocente__last_name',
                 'idDocente__names',
                 'id',
             )

    m = Matriculados.objects.filter(Estado=True).values(
            'idAlumno__names','id','idAlumno__first_name',
        'idAlumno__last_name', 'idAlumno__dni','idDocentexAsignatura_id'
        )

    return render(r, template, {'doc':d,'mat':m})