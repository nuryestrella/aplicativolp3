from django.apps import AppConfig


class AppreporteConfig(AppConfig):
    name = 'appReporte'
