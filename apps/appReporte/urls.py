from django.conf.urls import url
from .views import (listadoc,listaasignatura)


urlpatterns = [
    url(r'^reportes/AsignaturaxDocente', listadoc),
    url(r'^reportes/DocentexAsignatura', listaasignatura),


]