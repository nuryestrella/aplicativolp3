from django.shortcuts import render, HttpResponse
from django.http import HttpResponseBadRequest
from apps.Sistema_Aplicacion.models import (Asistencia,Clases, Matriculados, DocentexAsignatura,
                                            Semestre, SemestrexDocente, Asignatura, Evaluacion,)
from apps.security.models import Users
from .forms import formClase
import json

# Create your views here.
def asignaturaxdocente(r):
    template_name = "listasemestredocente.html"
    d = Semestre.objects.filter(Estado=True).order_by('-id')
    p = {'semestre':d}
    return render(r,template_name,p)

def listarasignatura(r):
    if r.method =="POST":
        s = DocentexAsignatura.objects.filter(idAsignatura_id=int(r.POST.get('idasignatura')),
                                              idSemestrexDocente_id=int(r.POST.get('idsemestredocente')), Estado=True)
        if s:
            msm ="ya se encuentra guardado"
            return HttpResponseBadRequest(msm)
        else:
            DocentexAsignatura(idAsignatura_id=int(r.POST.get('idasignatura')),idSemestrexDocente_id=int(r.POST.get('idsemestredocente'))).save()
            msm ="guardado correctamente"
            return HttpResponse(msm)

    else:
        template_name = "listaraAsignatura.html"
        id = int(r.GET.get("id"))
        idd = int(r.GET.get("iddocente"))
        ids = int(r.GET.get("ids"))
        docente = Users.objects.filter(status=True, profiles=11)
        asig    = Asignatura.objects.filter(Estado=True)
        d = DocentexAsignatura.objects.filter(Estado=True,idSemestrexDocente_id =id).values(
            'idAsignatura__Nombre','id','idAsignatura__Creditos','idAsignatura_id'
        )
        p = {'asignaturas':asig,'semestredocente':id,'DocentexAsignatura':d,'nombredocente':docente,'ids':ids}
        return render(r,template_name,p)
def listardocentes(r):
    if r.method =="POST":
        s = SemestrexDocente.objects.filter(idDocente_id=int(r.POST.get('iddocente')),idSemestre_id=int(r.POST.get('idsemestre')), Estado=True)
        if s:
            msm ="ya se encuentra guardado"
            return HttpResponseBadRequest(msm)
        else:
            SemestrexDocente(idDocente_id=int(r.POST.get('iddocente')),idSemestre_id=int(r.POST.get('idsemestre'))).save()
            msm ="guardado correctamente"
            return HttpResponse(msm)

    else:
        template_name = "listaradocente.html"
        idd = int(r.GET.get("idsemestre"))
        semestre = Semestre.objects.filter(id=idd)
        docentes = Users.objects.filter(status=True, profiles=11)
        d = SemestrexDocente.objects.filter(Estado=True,idSemestre=idd, idDocente__profiles=11).values('id',
            'idDocente__names','idDocente__first_name','idDocente__last_name','idDocente_id'
        )
        p = {'docentes':docentes,'SemestrexDocente':d,'nombresemestre':semestre}
        return render(r,template_name,p)

def eliminarasignaturaxdocente(r):
    idd = int(r.GET.get('iddd'))
    e = SemestrexDocente.objects.get(pk = idd)
    e.Estado = False
    e.save()
    return HttpResponse("eliminado correctamente")

def eliminarDocentexAsignatura(r):
    idd = int(r.GET.get('iddd'))
    e = DocentexAsignatura.objects.get(pk = idd)
    e.Estado = False
    e.save()
    return HttpResponse("eliminado correctamente")



### lista de  las asignaturas que fueron asiganadas al deocente+++

def listadeAsignatura(r):
    ids = Semestre.objects.filter(Estado=True).order_by('-id')[:1].values('id')
    idu = r.user.id
    d = DocentexAsignatura.objects.filter(Estado=True,idSemestrexDocente__idSemestre =ids,
                                          idSemestrexDocente__idDocente_id=idu).values(
            'idAsignatura__Nombre','id','idAsignatura__Creditos','idAsignatura_id'
        )
    template_name="cursosdeldocente.html"
    p = {'cursos':d}
    return render(r,template_name,p)

def listaAlumnos(r):
    d = Matriculados.objects.filter(Estado=True,
                                    idDocentexAsignatura_id =int(r.GET.get('id'))).values(
            'idAlumno__names','id','idAlumno__first_name',
        'idAlumno__last_name', 'idAlumno__dni'
        )

    template_name="alumnosdeldocente.html"
    p = {'alumnos':d}
    return render(r,template_name,p)

def listadeclases(r):
    d = Clases.objects.filter(Estado=True,
            idDocentexAsignatura_id =int(r.GET.get('id'))).values(
            'Tema','id','Descripcion',
            'fecha', 'idAula__Descripcion'
        )

    template_name="listadeclases.html"
    p = {'clases':d,'iddoc':r.GET.get('id')}
    return render(r,template_name,p)

def addclase(r):
    if r.method =='GET':
            f = formClase()
            t = "REGISTRAR"
            return render(r,"formularioClase.html",{'tipo':t,'formulario':f,'iddoc':r.GET.get('iddoc')})
    else:
        f =formClase(r.POST)

        if f.is_valid():
            f.save()
            return HttpResponse("Guardado correctamente")
        else:
            ee ={}
            for i in f.error:
                e =f.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))

def asistencia(r):
    d = Asistencia.objects.filter(Estado=True,
                                    idclases_id =int(r.GET.get('id'))).values(
            'idmatriculados__idAlumno__names','id','idmatriculados__idAlumno__first_name',
        'idmatriculados__idAlumno__last_name', 'idmatriculados__idAlumno__dni'
        )

    template_name="listaasistencia.html"
    p = {'asistencia':d,'idc':r.GET.get('idc')}
    return render(r,template_name,p)

def evaluacion(r):
    d = Matriculados.objects.filter(Estado=True,
                                    idDocentexAsignatura_id =int(r.GET.get('id'))).values(
            'idAlumno__names','id','idAlumno__first_name','idAlumno_id',
        'idAlumno__last_name', 'idAlumno__dni'
        )

    evv = Evaluacion.objects.filter(idmatriculados__idDocentexAsignatura=int(r.GET.get('id')))
    if not evv:
        for i in d:
            ee = Evaluacion()
            ee.idmatriculados_id = i["id"]
            ee.save()
    evv = Evaluacion.objects.filter(idmatriculados__idDocentexAsignatura=int(r.GET.get('id')))

    eva = []
    for i in d:
        for j in evv:
            if i["id"] == j.idmatriculados_id:
                eva.append({
                    'id': j.id,
                    'idAlumno__names' : i["idAlumno__names"],
                    'idAlumno__first_name' : i["idAlumno__first_name"],
                    'idAlumno__last_name'  : i["idAlumno__last_name"],
                    'idAlumno__dni'        : i["idAlumno__dni"],
                    'primerapractica'      : j.primerapractica,
                    'segundapractica'      : j.segundapractica,
                    'primerparcial'      : j.primerparcial ,
                    'segundoparcial'      : j.segundoparcial,
                    'trabajos'      : j.trabajos,
                    'notafinal'      : float(j.primerapractica *  0.15 + j.segundapractica *  0.15 + j.primerparcial *  0.25 + j.segundoparcial *  0.25 + j.trabajos *  0.20),
                })
    template_name="evaluacion.html"
    p = {'eva':eva,'idd':r.GET.get('id')}
    return render(r,template_name,p)

def nota(r):
    eval("Evaluacion.objects.filter(id = "+str(r.GET.get("id"))+").update("+r.GET.get("n")+" = "+str(r.GET.get("nota"))+")")
    return HttpResponse("actualizado")