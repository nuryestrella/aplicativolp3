from django.conf.urls import url
from .views import (asignaturaxdocente,listarasignatura,
                    listardocentes,eliminarasignaturaxdocente,eliminarDocentexAsignatura,
                    listadeAsignatura, listaAlumnos, listadeclases, addclase,
                    asistencia, evaluacion, nota
                    )


urlpatterns = [
    url(r'^asignaturadocente/asignaturaxdocente$', asignaturaxdocente),
    url(r'^asignaturadocente/asignaturaxdocente/listardocentes', listardocentes),
    url(r'^asignaturadocente/asignaturaxdocente/listarasignaturas$', listarasignatura),
    url(r'^asignaturadocente/asignaturaxdocente/eliminar$', eliminarasignaturaxdocente),
    url(r'^asignaturadocente/SemestrexDocente/eliminar$', eliminarDocentexAsignatura),

    url(r'^gestionasignatura/listaAsignatura$', listadeAsignatura),
    url(r'^gestionasignatura/listaAlumnos', listaAlumnos),
    url(r'^gestionasignatura/listadeclases', listadeclases),
    url(r'^gestionasignatura/addClase', addclase),
    url(r'^gestionasignatura/asistencia', asistencia),
    url(r'^gestionasignatura/evaluacion$', evaluacion),
    url(r'^gestionasignatura/nota$', nota),

]