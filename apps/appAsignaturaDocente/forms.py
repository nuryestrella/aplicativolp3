from django import forms
from apps.Sistema_Aplicacion.models import Clases

class formClase(forms.ModelForm):
    class Meta():
        model = Clases
        filter = ['Tema','Descripcion','fecha','idDocentexAsignatura','idAula' ]
        exclude = ('Estado',)
        widgets = {
            'Tema' : forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese Tema'}),
            'Descripcion' : forms.Textarea(attrs={'class': 'form-control input-sm', 'placeholder' : 'Ingrese su Descripcion'}),
            'fecha' : forms.DateInput(attrs={'class': 'form-control input-sm', 'type' : 'date'}),
            'idDocentexAsignatura' : forms.HiddenInput(),
        }

