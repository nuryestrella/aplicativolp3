from rest_framework import serializers
from django.contrib.auth import get_user_model
Users = get_user_model()
from .models import icons, profile, modules

class profileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = profile
        fields = ('description','id')

class iconosSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = icons
        fields =( 'id','clase','description',)

class modulesSeria(serializers.HyperlinkedModelSerializer):
    icon = iconosSerializer(read_only=True)
    class Meta:
        model = modules
        fields = ('id','description','url','icon','father')

class cuenseria(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= Users
        fields=('id','username','names','first_name','last_name','dni','telephone',)