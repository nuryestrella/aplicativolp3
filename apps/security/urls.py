from django.conf.urls import url
from django.contrib.auth.views import password_change,password_change_done
from apps.security.views.view_permission import permissionView, CambiarEstado, CambiarEstadoTotal
from apps.security.views.view_profile import deleteprofile ,profiles,editprofile
from apps.security.views.view_module import module, deletemodule,submodule,updatemodule,editamh, fonts

from apps.core.error import error404
from apps.security.views.view_account import cuentita, cuentaedi, datoscuenta,fotoperfil, guardaperfil, register_users, usuariosCompletado, list_users, update_users, about_users, deleteuser
from apps.security.views.view_login import login,LogOut, register
from apps.security.views.view_security import index



urlpatterns = [
    url(r'^login/$',login.as_view()),
    url(r'^register/$',register.as_view()),
    url(r'^salir/$', LogOut, name="cerrarsesion"),
    url(r'^fotoprofile/$', fotoperfil ),
    url(r'^$', index, name="index" ),
    url(r'^error404/$', error404.as_view(), name="error404" ),

    #perfiles
    url(r'^security/profile/$', profiles ),
    url(r'^security/profile/frmp/$', editprofile ),
    url(r'^security/profile/delete/(?P<id>\d+)/$', deleteprofile),

    #permission
    url(r'^security/permission/$', permissionView),
    url(r'^security/permission/CambiarEstado/$', CambiarEstado),
    url(r'^security/permission/CambiarEstadoTotal/$', CambiarEstadoTotal),

    #cuenta
    url(r'^security/guardaprofile$', guardaperfil),
    url(r'^security/datoscuenta$', datoscuenta),
    url(r'^password$', password_change, {'template_name': 'contra_usu.html'}),
    url(r'^password-hecho$', password_change_done, {'template_name': 'password-hecho.html'},name='password_change_done'),
    url(r'^security/cuenta$', cuentita, name="editcuenta"),
    url(r'^security/cuedi/$', cuentaedi ),

    #modules
    url(r'^security/modules/$', module),
    url(r'^security/modules/delete/(?P<id>\d+)/$', deletemodule),
    url(r'^security/modules/edit/(?P<id>\d+)/$', updatemodule),
    url(r'^security/submodules/(?P<id>\d+)/$', submodule ),
    url(r'^security/modules/fonts/$', fonts ),
    url(r'^security/modules/editmh/$', editamh ),

    #users
    #url(r'^security/guardarpadres$', guardarpadre ),
    url(r'^security/usuarios/add/$', register_users.as_view()),
    url(r'^security/usuarios/(?P<pk>[0-9]+)/$', update_users.as_view()),
    url(r'^security/usuarios/view/$', about_users.as_view()),
    url(r'^security/usuarios$', list_users.as_view()),
    url(r'^security/usuarios/delete/(?P<id>\d+)/$', deleteuser),
    url(r'^security/usuarios/completado$', usuariosCompletado, name="user-completado" ),

]
